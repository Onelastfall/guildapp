import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable, of} from 'rxjs';
import { map, mergeMap, switchMap} from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import {UserGuildId} from '../models/userGuildId';
import {combineLatest} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  auth$: Observable<any>;
  uId$: Observable<any>;
  constructor( private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this.auth$ = this.afAuth.authState;
    this.uId$ = this.afAuth.user.map(user => user.uid);
  }
  public authenticated() {
    return this.auth$.pipe( switchMap( user => {
      if (user) {
        return this.afs.doc(``).valueChanges();
      } else {
        return of(null);
      }
    }));
  }

  public login(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  public logout() {
    location.reload();
    return this.afAuth.auth.signOut().then(() => this.auth$ = null);
  }

  public getUid() {
    return this.uId$;
  }

  public getGuildId(): Observable<string> {
    return this.afs.collection<UserGuildId>('userGuildId', ref => ref.where('uId', '==', 'gQy4yQpNQnUmptvNU7qS01ygAue2'))
      .valueChanges()
      .pipe(
        map(infoArr => infoArr[0].guildId)
      );
  }

  public getUser() {
    const uid$ = this.getUid();
    const guildId$ = this.getGuildId();
    return combineLatest(uid$, guildId$).pipe(
      switchMap(
        arr => {
          const uId: string = arr[0].trim();
          const gId: string = arr[1].trim();
          return this.afs
            .collection(`Guilds`)
            .doc(gId)
            .collection("users", ref => ref.where("uId", "==", uId))
            .valueChanges();
        }
      )
    );
  }
}
