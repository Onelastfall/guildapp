import { Component } from '@angular/core';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  guildId = "q8EoPtNmg4X7PWMTUH9h";
  guild: Observable<any>;
  private guildDoc: AngularFirestoreDocument<any>;
  constructor(db: AngularFirestore) {
    this.guildDoc = db.doc(`Guilds/q8EoPtNmg4X7PWMTUH9h`);
    this.guild = this.guildDoc.valueChanges();
  }
}
