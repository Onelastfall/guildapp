import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {AppRoutingModule, routingComponents} from './app-routing.module';
import { AppComponent } from './app.component';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {ServiceModule} from './services/service.module';
import {AuthGuard} from './auth/auth.guard';
import {FormsModule} from '@angular/forms';
import {MDBBootstrapModulesPro} from 'ng-uikit-pro-standard';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { MyPageComponent } from './my-page/my-page.component';
import {RouterModule} from '@angular/router';
import { ProfileCardComponent } from './my-page/profile-card/profile-card.component';

@NgModule({
  declarations: [
    routingComponents,
    AppComponent,
    HomeComponent,
    MyPageComponent,
    ProfileCardComponent,
  ],
  imports: [
    MDBBootstrapModulesPro.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, "Lazy-guild-app"),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    ServiceModule
  ]
  ,
  providers: [ AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
