import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {UserGuildId} from '../models/userGuildId';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, AfterViewInit {

  userDoc;
  admin = false;

  constructor(
    private authService: AuthService,
    private elementRef: ElementRef,
    private afs: AngularFirestore
  ) { }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#E1BB80';
  }

  ngOnInit() {
    this.authService.getUser().subscribe( userArr => {
      this.admin = userArr[0].admin;
    });
  }

  goToLink(url: string) {
    console.log(url);
    window.open( ''+ url);
  }
  logout() {
    if (confirm("Are you sure you want to logout?")) {
      this.authService.logout();
    }
  }
}
