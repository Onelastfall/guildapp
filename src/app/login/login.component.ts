import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
  loginError = "";
  email = "";
  password = "";

  constructor(
    private elementRef: ElementRef,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#1F1405';
  }

  goToLink(url: string) {
    window.open(url, "_blank");
  }

  login() {
    this.authService.login(this.email, this.password).then(
    result => {
      this.router.navigate(['app']);
    }
    ).catch(
    error => {
      this.loginError = error.message;
    }
    );
  }
}
