import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-profile-card',
  templateUrl: './profile-card.component.html',
  styleUrls: ['./profile-card.component.scss']
})
export class ProfileCardComponent implements OnInit {

  classPic = {
    warrior: "https://img.rankedboost.com/wp-content/uploads/2019/05/WoW-Classic-Warrior-Guide.png",
    hunter: "https://img.rankedboost.com/wp-content/uploads/2019/05/WoW-Classic-Hunter-Guide.png",
    rogue: "https://img.rankedboost.com/wp-content/uploads/2019/05/WoW-Classic-Rogue-Guide.png",
    shaman: "https://i.ya-webdesign.com/images/wow-shaman-png-3.png",
    druid: "https://img.rankedboost.com/wp-content/uploads/2019/05/WoW-Classic-Druid-Guide.png",
    mage: "https://img.rankedboost.com/wp-content/uploads/2017/07/mage_13.png",
    priest: "https://img.u4n.com/images/wow-classic/career-icon/Priest.png",
    warlock: "https://img.rankedboost.com/wp-content/uploads/2019/05/WoW-Classic-Warlock-Guide.png"
  };

  user;

  constructor(
    private authService: AuthService
  ) {
    this.authService.getUser().subscribe( userArray => {
      this.user = userArray[0];
    });
  }

  ngOnInit() {
  }

}
