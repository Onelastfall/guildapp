import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AppComponent} from './app.component';
import {AuthGuard} from './auth/auth.guard';
import {SideMenuComponent} from './side-menu/side-menu.component';
import {HomeComponent} from './home/home.component';
import {MyPageComponent} from './my-page/my-page.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/app', pathMatch: 'full'},
  {path: 'app', component: SideMenuComponent,
    children: [
      {path: 'myPage', component: MyPageComponent},
      {path: '', component: HomeComponent},
    ],
    canActivate: [AuthGuard]
  },
  { path: '**', redirectTo: 'app', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent, SideMenuComponent];
