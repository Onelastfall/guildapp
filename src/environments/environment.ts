// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDFz_sk48D22pb67EsoE14ST6R5Oj8UoOY",
    authDomain: "guildapp-ebb8e.firebaseapp.com",
    databaseURL: "https://guildapp-ebb8e.firebaseio.com",
    projectId: "guildapp-ebb8e",
    storageBucket: "guildapp-ebb8e.appspot.com",
    messagingSenderId: "1062890340822",
    appId: "1:1062890340822:web:9b0fc65511c2311a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
